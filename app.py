from flask import request, render_template, flash, url_for, redirect, send_file
from flask_login import login_user, logout_user, login_required, current_user
from datetime import datetime

from models.user import User
from models.parameter import Parameter
from controller.controller import *
from controller.get_ftp_data import *
from controller.seller_central_upload import *
from controller.tradepeg_upload import *
from controller.clean_amazon_restock_report import *

from config import app, db, login_manager, bcrypt
from pathlib import Path


with app.app_context():
    db.create_all()


@login_manager.user_loader
def load_user(user_id):
    return db.session.get(User, user_id)


@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == "GET":
        try:
            if current_user.id:
                return redirect(url_for("home"))
        except:
            return render_template("login.html")
    elif request.method == "POST":
        username = request.form["username"]
        password = request.form["password"]

        if username and password:
            try:
                user = User.query.filter(User.username == username).first()

                status = User.verifyPassword(password, user.password, bcrypt)
                if status:
                    login_user(user)
                    return redirect(url_for("home"))
            except:
                flash("Incorrect username or password", "danger")
        else:
            flash("Please fill the required fields", "danger")

        return render_template("login.html")


@app.route("/logout", methods=["GET"])
@login_required
def logout():
    if request.method == "GET":
        logout_user()
        return redirect(url_for("index"))


@app.route("/home", methods=["GET"])
@login_required
def home():
    if request.method == "GET":
        return render_template("index.html", admin=current_user.isAdmin)


@app.route("/accounts", methods=["GET"])
@login_required
def accounts():
    if current_user.isAdmin == 1:
        if request.method == "GET":
            accounts = User.query.all()
            return render_template(
                "accounts.html", admin=current_user.isAdmin, accounts=accounts
            )


@app.route("/update-account", methods=["GET", "POST"])
@login_required
def updateAccount():
    if request.method == "GET":
        return render_template("user.html", admin=current_user.isAdmin)
    elif request.method == "POST":
        password = request.form["password"]
        newPassword = request.form["newPassword"]
        encryptedPassword = bcrypt.generate_password_hash(newPassword).decode("utf-8")

        updateUserPassword(
            db, current_user.username, password, encryptedPassword, bcrypt
        )
        return render_template("user.html", admin=current_user.isAdmin)


@app.route("/new-user", methods=["GET", "POST"])
@login_required
def newUser():
    if current_user.isAdmin == 1:
        if request.method == "GET":
            return render_template("register.html", admin=current_user.isAdmin)
        elif request.method == "POST":
            username = request.form["username"]
            password = request.form["password"]
            isAdmin = request.form["isAdmin"]
            encryptedPassword = bcrypt.generate_password_hash(password).decode("utf-8")

            if username and password:
                createNewUser(db, username, encryptedPassword, isAdmin)

            return render_template("register.html", admin=current_user.isAdmin)


@app.route(
    "/download/<string:fileName>/<string:directoryName>", methods=["GET", "POST"]
)
@login_required
def downloadFile(fileName, directoryName):
    filePath = f"data/{directoryName}/{fileName}"
    return send_file(filePath, as_attachment=True)


@app.route("/report-generate", methods=["GET", "POST"])
@login_required
def reportGenerate():
    if request.method == "GET":
        return render_template("report-generate.html", admin=current_user.isAdmin)
    elif request.method == "POST":
        inputFile = request.files["inputFile"]
        uploadedFile = uploadFile(inputFile)
        country = request.form["country"]
        timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")
        outputFile = f"cleaned_restock_report_{timestamp}.txt"
        result = generateCleanReportFile(
            Path(__file__).parent / "data" / "inputs" / uploadedFile,
            Path(__file__).parent / "data" / "outputs" / outputFile,
            getParameters(),
            country
        )

        fileName = None
        directoryName = None
        if result:
            directoryName = "outputs"
            fileName = outputFile

        return render_template(
            "report-generate.html",
            admin=current_user.isAdmin,
            fileName=fileName,
            directoryName=directoryName,
        )


@app.route("/amazon-generate", methods=["GET", "POST"])
@login_required
def amazonGenerate():
    if request.method == "GET":
        return render_template("amazon-generate.html", admin=current_user.isAdmin)
    elif request.method == "POST":
        restockFile = request.files["restockFile"]
        tradepegFile = request.files["tradepegFile"]
        restockUploadedFile = uploadFile(restockFile)
        tradepegUploadedFile = uploadFile(tradepegFile)
        result = generateAmazonSellerFile(
            Path(__file__).parent / "data" / "inputs" / restockUploadedFile,
            Path(__file__).parent / "data" / "inputs" / tradepegUploadedFile,
            Path(__file__).parent / "data" / "outputs" / "sc_shipment_upload_file.txt",
            Path(__file__).parent / "data" / "outputs" / "cleaned_restock_report.txt",
            getParameters(),
        )

        fileName = None
        directoryName = None
        if result:
            directoryName = "outputs"
            fileName = "sc_shipment_upload_file.txt"

        return render_template(
            "amazon-generate.html",
            admin=current_user.isAdmin,
            fileName=fileName,
            directoryName=directoryName,
        )


@app.route("/tradepeg-generate", methods=["GET", "POST"])
@login_required
def tradepegGenerate():
    if request.method == "GET":
        return render_template("tradepeg-generate.html", admin=current_user.isAdmin)
    elif request.method == "POST":
        pickListFile = request.files["pickListFile"]
        tradepegFile = request.files["tradepegFile"]
        pickListUploadedFile = uploadFile(pickListFile)
        tradepegUploadedFile = uploadFile(tradepegFile)
        result = generateTradepegFile(
            Path(__file__).parent / "data" / "inputs" / tradepegUploadedFile,
            Path(__file__).parent / "data" / "outputs" / "tradepeg_upload.csv",
            Path(__file__).parent / "data" / "inputs" / pickListUploadedFile,
        )

        fileName = None
        directoryName = None
        if result:
            directoryName = "outputs"
            fileName = "tradepeg_upload.csv"

        return render_template(
            "tradepeg-generate.html",
            admin=current_user.isAdmin,
            fileName=fileName,
            directoryName=directoryName,
        )


@app.route("/settings", methods=["GET", "POST"])
@login_required
def settings():
    if current_user.isAdmin == 1:
        parameters = Parameter.query.all()
        if request.method == "GET":
            return render_template(
                "settings.html", admin=current_user.isAdmin, parameters=parameters
            )
        elif request.method == "POST":
            parameterName = request.form["parameterName"]
            parameterValue = request.form["parameterValue"]

            if parameterName and parameterValue:
                createNewPameter(db, parameterName, parameterValue)
                parameters = Parameter.query.all()

            return render_template(
                "settings.html", admin=current_user.isAdmin, parameters=parameters
            )


@app.route("/update-parameter/<int:id>", methods=["GET", "POST"])
@login_required
def updateParameter(id):
    if current_user.isAdmin == 1:
        if request.method == "GET":
            parameter = Parameter.query.filter(Parameter.id == id).first()
            return render_template(
                "update-parameter.html", admin=current_user.isAdmin, parameter=parameter
            )
        elif request.method == "POST":
            parameterName = request.form["parameterName"]
            parameterValue = request.form["parameterValue"]

            if parameterName and parameterValue:
                setParameter(db, id, parameterName, parameterValue)
                parameter = Parameter.query.filter(Parameter.id == id).first()

            return render_template(
                "update-parameter.html", admin=current_user.isAdmin, parameter=parameter
            )


@app.route("/delete-parameter/<int:id>")
@login_required
def deleteParameter(id):
    if current_user.isAdmin == 1:
        try:
            Parameter.query.filter(Parameter.id == id).delete()
            db.session.commit()
        except Exception as e:
            print(e)
            db.session.rollback()
            flash("Something went wrong, please try again", "danger")

        return redirect(url_for("settings"))


if __name__ == "__main__":
    app.run(debug=False)
