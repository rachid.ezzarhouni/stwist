from config import db
from flask_login import UserMixin
from flask_bcrypt import Bcrypt
from flask import flash


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64))
    password = db.Column(db.String(64))
    isAdmin = db.Column(db.Integer)

    def __init__(self, username: str, password: str, isAdmin: int) -> None:
        self.username = username
        self.password = password
        self.isAdmin = isAdmin

    def verifyPassword(password: str, encyptedPassword: str, bcrypt: Bcrypt) -> bool:
        if bcrypt.check_password_hash(encyptedPassword, password):
            return True
        else:
            flash('Incorrect user credentials, please try again', 'danger')
            return False
