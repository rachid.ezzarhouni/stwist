from config import db   

class Parameter(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    value = db.Column(db.String(64))

    def __init__(self, name: str, value: str) -> None:
        self.name = name
        self.value = value