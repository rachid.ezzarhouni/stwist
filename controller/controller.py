from werkzeug.datastructures import FileStorage
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask import flash
from datetime import datetime
from models.user import User
from models.parameter import Parameter
from pathlib import Path
import os


def createNewUser(db: SQLAlchemy, username: str, encryptedPassword: str, isAdmin: int):
    try:
        user = User(username, encryptedPassword, isAdmin)
        db.session.add(user)
        db.session.commit()
        flash("Successfully registered a new user", "success")
    except Exception as e:
        db.session.rollback()
        flash("Something went wrong, please try again", "danger")


def updateUserPassword(
    db: SQLAlchemy, username: str, password: str, encyptedPassword: str, bcrypt: Bcrypt
):
    user = User.query.filter(User.username == username).first()
    status = User.verifyPassword(password, user.password, bcrypt)

    if status:
        user.password = encyptedPassword
        db.session.commit()
        flash("Successfully changed.", "success")


def createNewPameter(db: SQLAlchemy, name: str, value: str) -> None:
    try:
        parameter = Parameter(name, value)
        db.session.add(parameter)
        db.session.commit()
        flash("Successfully saved new parameter", "success")
    except Exception as e:
        db.session.rollback()
        flash("Something went wrong, please try again", "danger")


def setParameter(db: SQLAlchemy, id: int, name: str, value: str) -> None:
    try:
        parameter = Parameter.query.filter(Parameter.id == id).first()
        parameter.name = name
        parameter.value = value
        db.session.commit()
        flash("Successfully changed.", "success")
        return parameter
    except Exception as e:
        db.session.rollback()
        flash("Something went wrong, please try again", "danger")


def uploadFile(fileUpload: FileStorage) -> str:
    try:
        timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")
        file_name, file_extension = os.path.splitext(fileUpload.filename)
        file_name_with_timestamp = f"{file_name}_{timestamp}{file_extension}"
        if fileUpload:
            current_directory = Path(__file__).parent.resolve()
            save_path = (
                current_directory / ".." / "data" / "inputs" / file_name_with_timestamp
            )
            fileUpload.save(str(save_path))
        return file_name_with_timestamp
    except Exception as e:
        print(f"Error occurred: {str(e)}")
        return "error"


def getParameters() -> dict:
    return {
        "Recommended replenishment quantity": int(
            Parameter.query.filter(
                Parameter.name == "Recommended replenishment quantity"
            )
            .first()
            .value
        )
    }
