import pandas as pd
from dotenv import load_dotenv
from flask import flash
from pathlib import Path
from controller.utils import FBM
from controller.clean_amazon_restock_report import clean_amazon_restock_report
from controller.get_ftp_data import get_tradepeg_file


def cap_stock(row):
    if row["Stock Level"] < row["Recommended replenishment qty"]:
        return row["Stock Level"]
    else:
        return row["Recommended replenishment qty"]


def get_restock_file():
    """gets the restock file"""
    pass


def make_shipment_file(
    restock_file: Path, tradepeg_file: Path, output_file="output.txt"
):
    """makes a new shipment file"""
    # Read the data from the first input file (Amazon Restock Report)
    amazon_data = pd.read_csv(restock_file, delimiter="\t")

    # Perform data filtering - Include only rows where 'Country/Region Code' is 'GB' and 'Recommended replenishment qty' is greater than zero
    amazon_data = amazon_data[
        (amazon_data["Country"] == "GB")
        & (amazon_data["Recommended replenishment qty"] > 0)
    ]
    amazon_data["FBM"] = amazon_data["Merchant SKU"].apply(FBM)
    amazon_data = amazon_data[amazon_data["FBM"] == True]

    # Read the data from the second input file (Tradepeg stock levels)
    tradepeg_data = pd.read_csv(tradepeg_file, delimiter="\t")
    tradepeg_data = tradepeg_data[
        tradepeg_data["Marketplace"] == "Sense Group - Amazon UK"
    ]
    tradepeg_data = tradepeg_data[tradepeg_data["Avl"] > 0]
    tradepeg_data["FBM"] = tradepeg_data["MPSku"].apply(FBM)
    tradepeg_data = tradepeg_data[tradepeg_data["FBM"] == True]

    # Merge the data from both files based on a common column (e.g., SKU)
    merged_data = pd.merge(
        amazon_data,
        tradepeg_data,
        left_on="ASIN",
        right_on="MarketplaceItemId",
        how="left",
    )
    
    duplicate_asins_found = merged_data["ASIN"].duplicated().any()
    # Select only the necessary columns for the output file
    output_data = merged_data[["Merchant SKU", "Recommended replenishment qty", "Avl"]]
    output_data = output_data.drop_duplicates(keep='first')
    
    # Add constant values to 'Prep owner' and 'Labeling owner'
    output_data["Prep owner"] = "Seller"
    output_data["Labeling owner"] = "Seller"

    # replace nan with 0 for stock level
    output_data["Stock Level"] = output_data["Avl"].fillna(0)
    output_data.drop(columns=["Avl"], inplace=True)
    
    # cap the recommended replenishment to the stock level
    output_data["Recommended replenishment qty"] = output_data.apply(cap_stock, axis=1)
    # output_data = merged_data[['Merchant SKU', 'Recommended replenishment qty']]

    # Rename the column header from 'Recommended replenishment qty' to 'Quantity'
    output_data.rename(
        columns={"Recommended replenishment qty": "Quantity"}, inplace=True
    )

    output_data = output_data[output_data["Quantity"] > 0]
    # Round 'Recommended replenishment qty' column to a whole number
    output_data["Quantity"] = output_data["Quantity"].apply(lambda x: int(x))

    output_data = output_data[
        ["Merchant SKU", "Quantity", "Prep owner", "Labeling owner"]
    ]

    # Add the header rows above the column names
    header_rows = [
        ["Please review the Example tab before you complete this sheet", "", "", ""],
        ["", "", "", ""],
        ["Default prep owner", "Seller"],
        ["Default labeling owner", "Seller"],
        ["", "", "", ""],
        ["", "", "", ""],
        ["", "", "Optional", ""],
    ]

    # Convert header_rows to a DataFrame and concatenate it with the output_data DataFrame
    with open(output_file, "w") as f:
        # write the lines from the header
        for header_row in header_rows:
            f.write("\t".join(header_row) + "\n")
        # write the column names
        f.write("\t".join(output_data.columns) + "\n")
        # write all the output data
        for index, row in output_data.iterrows():
            textrow = [str(x) for x in row]
            f.write("\t".join(textrow) + "\n")

    if duplicate_asins_found:
        flash(
            "OUTPUT FILE IS INVALID. Duplicate ASINs found in the output file. OUTPUT FILE IS INVALID",
            "warning",
        )
    else:
        flash("Amazon Seller File Successfully Generated", "success")


def generateAmazonSellerFile(
    restockFile: Path,
    tradepegFile: Path,
    outputFile: Path,
    outputFileReport: Path,
    parameters: dict,
) -> int:
    load_dotenv()
    try:
        TRADEPEG_STOCK_LEVEL = get_tradepeg_file()
    except:
        TRADEPEG_STOCK_LEVEL = tradepegFile

    try:
        RESTOCK_REPORT_INPUT = restockFile
        OUTPUT_FILE = outputFile
        cleaned_restock_report = clean_amazon_restock_report(
            RESTOCK_REPORT_INPUT, outputFileReport, parameters, "", "PF"
        )
        make_shipment_file(cleaned_restock_report, TRADEPEG_STOCK_LEVEL, OUTPUT_FILE)
        return 1
    except Exception as e:
        flash(f"Something went wrong, please try again {e}", "danger")
        return 0


# tradepeg_data[tradepeg_data['MarketplaceItemId']=="B07HCHXTJK"]
# amazon_data[amazon_data['ASIN']=="B07HCHXTJK"]["Merchant SKU"]
