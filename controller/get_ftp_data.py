from ftplib import FTP_TLS
import os
from pathlib import Path
#import ssl

def get_tradepeg_file(filename:str = "all_mp_products.csv")->Path:
    """gets the tradepeg file"""
    local_file_path = Path(__file__).parent / "data" / "inputs" / filename
    ftps_server = os.getenv("FTPS_SERVER")
    username = os.getenv("USERNAME")
    password = os.getenv("PASSWORD")
    remote_file_path = 'all_mp_products.csv'
    
    ftps = FTP_TLS()
    #ftps.context = ssl._create_unverified_context()
    ftps.connect(ftps_server, 21) 
    ftps.login(username, password)
    ftps.prot_p()

    with open(local_file_path, 'wb') as local_file:
        ftps.retrbinary('RETR ' + remote_file_path, local_file.write)
    ftps.quit()
    return local_file_path