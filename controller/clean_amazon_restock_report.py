from pathlib import Path
from csv import DictReader
from flask import flash


def clean_amazon_restock_report(
    file_path: Path,
    output_file: Path,
    parameters: dict,
    country: str = "",
    merchant_sku: str = "",
) -> Path:
    data = []

    with open(file_path, "r", encoding="utf-8") as file:
        header = file.readline().strip().split("\t")
        reader = DictReader(file, delimiter="\t", fieldnames=header)
        for record in reader:
            if (not country or record["Country"] == country) and (
                not merchant_sku or record["Merchant SKU"].startswith(merchant_sku)
            ):
                data.append(record)

    counter = 1
    for record in data:
        counter += 1
        units_sold = int(record.get("Units Sold Last 30 Days", 0))
        total_units = int(record.get("Total Units", 0))
        if units_sold > 0 and total_units > 0:
            recommended_replenishment_qty = max(units_sold - total_units, 0)
            record["Recommended replenishment qty"] = recommended_replenishment_qty
        elif units_sold > 0 and total_units == 0:
            recommended_replenishment_qty = units_sold * 2
            record["Recommended replenishment qty"] = recommended_replenishment_qty
        elif units_sold == 0 and total_units > 0:
            record["Recommended replenishment qty"] = 0
        elif units_sold == 0 and total_units == 0:
            record["Recommended replenishment qty"] = parameters[
                "Recommended replenishment quantity"
            ]
        else:
            raise Exception(f"Uncaught exception on row {counter}")
        record["Recommended replenishment qty"] = str(
            max(record["Recommended replenishment qty"], 0)
        )

    with open(output_file, "w", encoding="utf-8") as file:
        file.write("\t".join(header) + "\n")
        for record in data:
            file.write("\t".join(str(record[col]) for col in header) + "\n")

    return output_file


def generateCleanReportFile(
    inputFile: Path, outputFile: Path, parameters: dict, country: str
) -> None:
    try:
        clean_amazon_restock_report(inputFile, outputFile, parameters, country, "PF_")
        flash("Clean Report File Successfully Generated", "success")
        return 1
    except Exception as e:
        flash(f"Something went wrong, please try again {e}", "danger")
        return 0
