import pandas as pd
from pathlib import Path
from dotenv import load_dotenv
from flask import flash
from controller.get_ftp_data import get_tradepeg_file
from controller.utils import FBM


def process_2(tradepeg_file: Path, pick_list: Path, output_file: Path) -> None:
    # get tradepeg data
    tradepeg_data = pd.read_csv(tradepeg_file, delimiter="\t")
    tradepeg_data = tradepeg_data[
        tradepeg_data["Marketplace"] == "Sense Group - Amazon UK"
    ]
    tradepeg_data = tradepeg_data[["MPSku", "MarketplaceItemId", "LinkedSku"]]
    # filter tradepeg data to get rid of FBM skus
    tradepeg_data["FBM"] = tradepeg_data["MPSku"].apply(FBM)
    tradepeg_data = tradepeg_data[tradepeg_data["FBM"] == True]
    # get pick list
    pick_list = pd.read_csv(pick_list, delimiter=",", skiprows=5)
    pick_list = pick_list[["ASIN", "Quantity"]]
    # merge datasets
    output_data = pd.merge(
        pick_list,
        tradepeg_data,
        left_on="ASIN",
        right_on="MarketplaceItemId",
        how="left",
    )
    # format output file
    duplicate_asins_found = output_data["ASIN"].duplicated().any()
    output_data = output_data[["LinkedSku", "Quantity"]]
    output_data.columns = ["Identifier", "Qty"]
    # output_data.rename(columns={'Linked part code': 'Identifier'}, inplace=True)

    if duplicate_asins_found:
        output_data.to_csv(output_file, index=False, sep=",")
        flash("Duplicate ASINs found in pick list", "warning")
    else:
        summed_data = output_data.groupby("Identifier", as_index=False)["Qty"].sum()
        summed_data.to_csv(output_file, index=False, sep=",")
        flash("Tradepeg File Successfully Generated", "success")


def generateTradepegFile(
    tradepegStocklevel: Path, outputFile: Path, pickList: Path
) -> int:
    load_dotenv()
    try:
        TRADEPEG_STOCK_LEVEL = get_tradepeg_file()
    except:
        TRADEPEG_STOCK_LEVEL = tradepegStocklevel

    try:
        process_2(
            tradepeg_file=TRADEPEG_STOCK_LEVEL,
            pick_list=pickList,
            output_file=outputFile,
        )
        
        return 1
    except:
        flash("Something went wrong, please try again", "danger")
        return 0
