from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_bcrypt import Bcrypt

db = SQLAlchemy()

app = Flask(__name__)
app.config["SECRET_KEY"] = "Ae5pENEMQwSsmkQT9rN1mcD08nBPRroZ"
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///stwist.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

login_manager = LoginManager(app)
login_manager.init_app(app)

bcrypt = Bcrypt(app)
db.init_app(app)